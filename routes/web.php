<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('users', 'UserController');

Route::resource('users', 'UserController');

Route::group(['prefix' => 'members'], function () {
    Route::resource('sellers', 'Members\sellerController', ["as" => 'members']);
    Route::get('courses/displayV/{id}', 'Members\courseController@display')->name('courses.display');
    //members.courses.display
    Route::resource('courses', 'Members\courseController', ["as" => 'members']);
    Route::resource('customers', 'Members\customerController', ["as" => 'members']);
    Route::resource('reservationOrders', 'Members\reservationOrderController', ["as" => 'members']);
    Route::resource('purchaseOrders', 'Members\purchaseOrderController', ["as" => 'members']);
});

Route::resource('api', 'apiController');

Route::resource('drons', 'dronController');

Route::resource('missions', 'missionController');

Route::resource('data', 'dataController');