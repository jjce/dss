<?php

namespace App\Http\Controllers\Members;

use App\Http\Requests\Members\CreatereservationOrderRequest;
use App\Http\Requests\Members\UpdatereservationOrderRequest;
use App\Repositories\Members\reservationOrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class reservationOrderController extends AppBaseController
{
    /** @var  reservationOrderRepository */
    private $reservationOrderRepository;

    public function __construct(reservationOrderRepository $reservationOrderRepo)
    {
        $this->reservationOrderRepository = $reservationOrderRepo;
    }

    /**
     * Display a listing of the reservationOrder.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reservationOrderRepository->pushCriteria(new RequestCriteria($request));
        $reservationOrders = $this->reservationOrderRepository->all();

        return view('members.reservation_orders.index')
            ->with('reservationOrders', $reservationOrders);
    }

    /**
     * Show the form for creating a new reservationOrder.
     *
     * @return Response
     */
    public function create()
    {
        return view('members.reservation_orders.create');
    }

    /**
     * Store a newly created reservationOrder in storage.
     *
     * @param CreatereservationOrderRequest $request
     *
     * @return Response
     */
    public function store(CreatereservationOrderRequest $request)
    {
        $input = $request->all();

        $reservationOrder = $this->reservationOrderRepository->create($input);

        Flash::success('Reservation Order saved successfully.');

        return redirect(route('members.reservationOrders.index'));
    }

    /**
     * Display the specified reservationOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reservationOrder = $this->reservationOrderRepository->findWithoutFail($id);

        if (empty($reservationOrder)) {
            Flash::error('Reservation Order not found');

            return redirect(route('members.reservationOrders.index'));
        }

        return view('members.reservation_orders.show')->with('reservationOrder', $reservationOrder);
    }

    /**
     * Show the form for editing the specified reservationOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reservationOrder = $this->reservationOrderRepository->findWithoutFail($id);

        if (empty($reservationOrder)) {
            Flash::error('Reservation Order not found');

            return redirect(route('members.reservationOrders.index'));
        }

        return view('members.reservation_orders.edit')->with('reservationOrder', $reservationOrder);
    }

    /**
     * Update the specified reservationOrder in storage.
     *
     * @param  int              $id
     * @param UpdatereservationOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatereservationOrderRequest $request)
    {
        $reservationOrder = $this->reservationOrderRepository->findWithoutFail($id);

        if (empty($reservationOrder)) {
            Flash::error('Reservation Order not found');

            return redirect(route('members.reservationOrders.index'));
        }

        $reservationOrder = $this->reservationOrderRepository->update($request->all(), $id);

        Flash::success('Reservation Order updated successfully.');

        return redirect(route('members.reservationOrders.index'));
    }

    /**
     * Remove the specified reservationOrder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reservationOrder = $this->reservationOrderRepository->findWithoutFail($id);

        if (empty($reservationOrder)) {
            Flash::error('Reservation Order not found');

            return redirect(route('members.reservationOrders.index'));
        }

        $this->reservationOrderRepository->delete($id);

        Flash::success('Reservation Order deleted successfully.');

        return redirect(route('members.reservationOrders.index'));
    }
}
