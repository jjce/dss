<?php

namespace App\Http\Controllers\Members;

use App\Http\Requests\Members\CreatepurchaseOrderRequest;
use App\Http\Requests\Members\UpdatepurchaseOrderRequest;
use App\Repositories\Members\purchaseOrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class purchaseOrderController extends AppBaseController
{
    /** @var  purchaseOrderRepository */
    private $purchaseOrderRepository;

    public function __construct(purchaseOrderRepository $purchaseOrderRepo)
    {
        $this->purchaseOrderRepository = $purchaseOrderRepo;
    }

    /**
     * Display a listing of the purchaseOrder.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->purchaseOrderRepository->pushCriteria(new RequestCriteria($request));
        $purchaseOrders = $this->purchaseOrderRepository->all();

        return view('members.purchase_orders.index')
            ->with('purchaseOrders', $purchaseOrders);
    }

    /**
     * Show the form for creating a new purchaseOrder.
     *
     * @return Response
     */
    public function create()
    {
        return view('members.purchase_orders.create');
    }

    /**
     * Store a newly created purchaseOrder in storage.
     *
     * @param CreatepurchaseOrderRequest $request
     *
     * @return Response
     */
    public function store(CreatepurchaseOrderRequest $request)
    {
        $input = $request->all();

        $purchaseOrder = $this->purchaseOrderRepository->create($input);

        Flash::success('Purchase Order saved successfully.');

        return redirect(route('members.purchaseOrders.index'));
    }

    /**
     * Display the specified purchaseOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('members.purchaseOrders.index'));
        }

        return view('members.purchase_orders.show')->with('purchaseOrder', $purchaseOrder);
    }

    /**
     * Show the form for editing the specified purchaseOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('members.purchaseOrders.index'));
        }

        return view('members.purchase_orders.edit')->with('purchaseOrder', $purchaseOrder);
    }

    /**
     * Update the specified purchaseOrder in storage.
     *
     * @param  int              $id
     * @param UpdatepurchaseOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepurchaseOrderRequest $request)
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('members.purchaseOrders.index'));
        }

        $purchaseOrder = $this->purchaseOrderRepository->update($request->all(), $id);

        Flash::success('Purchase Order updated successfully.');

        return redirect(route('members.purchaseOrders.index'));
    }

    /**
     * Remove the specified purchaseOrder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $purchaseOrder = $this->purchaseOrderRepository->findWithoutFail($id);

        if (empty($purchaseOrder)) {
            Flash::error('Purchase Order not found');

            return redirect(route('members.purchaseOrders.index'));
        }

        $this->purchaseOrderRepository->delete($id);

        Flash::success('Purchase Order deleted successfully.');

        return redirect(route('members.purchaseOrders.index'));
    }
}
