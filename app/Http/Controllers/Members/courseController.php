<?php

namespace App\Http\Controllers\Members;

use App\Http\Requests\Members\CreatecourseRequest;
use App\Http\Requests\Members\UpdatecourseRequest;
use App\Repositories\Members\courseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class courseController extends AppBaseController
{
    /** @var  courseRepository */
    private $courseRepository;

    public function __construct(courseRepository $courseRepo)
    {
        $this->courseRepository = $courseRepo;
    }

    /**
     * Display a listing of the course.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->courseRepository->pushCriteria(new RequestCriteria($request));
        $courses = $this->courseRepository->all();

        return view('members.courses.index')
            ->with('courses', $courses);
    }

    /**
     * Show the form for creating a new course.
     *
     * @return Response
     */
    public function create()
    {
        return view('members.courses.create');
    }

    /**
     * Store a newly created course in storage.
     *
     * @param CreatecourseRequest $request
     *
     * @return Response
     */
    public function store(CreatecourseRequest $request)
    {
        $input = $request->all();

        $course = $this->courseRepository->create($input);

        Flash::success('Course saved successfully.');

        return redirect(route('members.courses.index'));
    }

    /**
     * Display the specified course.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $course = $this->courseRepository->findWithoutFail($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('members.courses.index'));
        }

        return view('members.courses.show')->with('course', $course);
    }

    /**
     * Display the specified course.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function display($id)
    {
        $course= "testing...";
        return view('members.user.courses.course-details')->with('course', $id);
    }

    /**
     * Show the form for editing the specified course.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $course = $this->courseRepository->findWithoutFail($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('members.courses.index'));
        }

        return view('members.courses.edit')->with('course', $course);
    }

    /**
     * Update the specified course in storage.
     *
     * @param  int              $id
     * @param UpdatecourseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecourseRequest $request)
    {
        $course = $this->courseRepository->findWithoutFail($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('members.courses.index'));
        }

        $course = $this->courseRepository->update($request->all(), $id);

        Flash::success('Course updated successfully.');

        return redirect(route('members.courses.index'));
    }

    /**
     * Remove the specified course from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $course = $this->courseRepository->findWithoutFail($id);

        if (empty($course)) {
            Flash::error('Course not found');

            return redirect(route('members.courses.index'));
        }

        $this->courseRepository->delete($id);

        Flash::success('Course deleted successfully.');

        return redirect(route('members.courses.index'));
    }
}
