<?php

namespace App\Http\Controllers\Members;

use App\Http\Requests\Members\CreatesellerRequest;
use App\Http\Requests\Members\UpdatesellerRequest;
use App\Repositories\Members\sellerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class sellerController extends AppBaseController
{
    /** @var  sellerRepository */
    private $sellerRepository;

    public function __construct(sellerRepository $sellerRepo)
    {
        $this->sellerRepository = $sellerRepo;
    }

    /**
     * Display a listing of the seller.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sellerRepository->pushCriteria(new RequestCriteria($request));
        $sellers = $this->sellerRepository->all();

        return view('members.sellers.index')
            ->with('sellers', $sellers);
    }

    /**
     * Show the form for creating a new seller.
     *
     * @return Response
     */
    public function create()
    {
        return view('members.sellers.create');
    }

    /**
     * Store a newly created seller in storage.
     *
     * @param CreatesellerRequest $request
     *
     * @return Response
     */
    public function store(CreatesellerRequest $request)
    {
        $input = $request->all();

        $seller = $this->sellerRepository->create($input);

        Flash::success('Seller saved successfully.');

        return redirect(route('members.sellers.index'));
    }

    /**
     * Display the specified seller.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $seller = $this->sellerRepository->findWithoutFail($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('members.sellers.index'));
        }

        return view('members.sellers.show')->with('seller', $seller);
    }

    /**
     * Show the form for editing the specified seller.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $seller = $this->sellerRepository->findWithoutFail($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('members.sellers.index'));
        }

        return view('members.sellers.edit')->with('seller', $seller);
    }

    /**
     * Update the specified seller in storage.
     *
     * @param  int              $id
     * @param UpdatesellerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesellerRequest $request)
    {
        $seller = $this->sellerRepository->findWithoutFail($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('members.sellers.index'));
        }

        $seller = $this->sellerRepository->update($request->all(), $id);

        Flash::success('Seller updated successfully.');

        return redirect(route('members.sellers.index'));
    }

    /**
     * Remove the specified seller from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $seller = $this->sellerRepository->findWithoutFail($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('members.sellers.index'));
        }

        $this->sellerRepository->delete($id);

        Flash::success('Seller deleted successfully.');

        return redirect(route('members.sellers.index'));
    }
}
