<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatemissionRequest;
use App\Http\Requests\UpdatemissionRequest;
use App\Repositories\missionRepository;
use App\Repositories\dataRepository;
use App\Models\data;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class missionController extends AppBaseController
{
    /** @var  missionRepository */
    private $missionRepository;
    private $dataRepository;

    public function __construct(missionRepository $missionRepo, dataRepository $dataRepo)
    {
        $this->missionRepository = $missionRepo;
        $this->dataRepository = $dataRepo;
    }

    /**
     * Display a listing of the mission.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->missionRepository->pushCriteria(new RequestCriteria($request));
        $missions = $this->missionRepository->findWhere($request->toArray());

        return view('missions.index')
            ->with('missions', $missions);
    }

    /**
     * Show the form for creating a new mission.
     *
     * @return Response
     */
    public function create()
    {
        return view('missions.create');
    }

    /**
     * Store a newly created mission in storage.
     *
     * @param CreatemissionRequest $request
     *
     * @return Response
     */
    public function store(CreatemissionRequest $request)
    {
        $input = $request->all();

        $mission = $this->missionRepository->create($input);

        Flash::success('Mission saved successfully.');

        return redirect(route('missions.index'));
    }

    /**
     * Display the specified mission.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mission = $this->missionRepository->findWithoutFail($id);

        $data =data::where('id_mision',$id)->orderBy('id','desc')->get();


        if (empty($mission)) {
            Flash::error('Mission not found');

            return redirect(route('missions.index'));
        }

        return view('missions.show')
            ->with('mission', $mission)
            ->with('data', $data);
    }

    /**
     * Show the form for editing the specified mission.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mission = $this->missionRepository->findWithoutFail($id);

        if (empty($mission)) {
            Flash::error('Mission not found');

            return redirect(route('missions.index'));
        }

        return view('missions.edit')->with('mission', $mission);
    }

    /**
     * Update the specified mission in storage.
     *
     * @param  int              $id
     * @param UpdatemissionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemissionRequest $request)
    {
        $mission = $this->missionRepository->findWithoutFail($id);

        if (empty($mission)) {
            Flash::error('Mission not found');

            return redirect(route('missions.index'));
        }

        $mission = $this->missionRepository->update($request->all(), $id);

        Flash::success('Mission updated successfully.');

        return redirect(route('missions.index'));
    }

    /**
     * Remove the specified mission from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mission = $this->missionRepository->findWithoutFail($id);

        if (empty($mission)) {
            Flash::error('Mission not found');

            return redirect(route('missions.index'));
        }

        $this->missionRepository->delete($id);

        Flash::success('Mission deleted successfully.');

        return redirect(route('missions.index'));
    }
}
