<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatedronRequest;
use App\Http\Requests\UpdatedronRequest;
use App\Repositories\dronRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class dronController extends AppBaseController
{
    /** @var  dronRepository */
    private $dronRepository;

    public function __construct(dronRepository $dronRepo)
    {
        $this->dronRepository = $dronRepo;
    }

    /**
     * Display a listing of the dron.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->dronRepository->pushCriteria(new RequestCriteria($request));
        $drons = $this->dronRepository->all();

        return view('drons.index')
            ->with('drons', $drons);
    }

    /**
     * Show the form for creating a new dron.
     *
     * @return Response
     */
    public function create()
    {
        return view('drons.create');
    }

    /**
     * Store a newly created dron in storage.
     *
     * @param CreatedronRequest $request
     *
     * @return Response
     */
    public function store(CreatedronRequest $request)
    {
        $input = $request->all();

        $dron = $this->dronRepository->create($input);

        Flash::success('Dron saved successfully.');

        return redirect(route('drons.index'));
    }

    /**
     * Display the specified dron.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dron = $this->dronRepository->findWithoutFail($id);

        if (empty($dron)) {
            Flash::error('Dron not found');

            return redirect(route('drons.index'));
        }

        return view('drons.show')->with('dron', $dron);
    }

    /**
     * Show the form for editing the specified dron.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dron = $this->dronRepository->findWithoutFail($id);

        if (empty($dron)) {
            Flash::error('Dron not found');

            return redirect(route('drons.index'));
        }

        return view('drons.edit')->with('dron', $dron);
    }

    /**
     * Update the specified dron in storage.
     *
     * @param  int              $id
     * @param UpdatedronRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedronRequest $request)
    {
        $dron = $this->dronRepository->findWithoutFail($id);

        if (empty($dron)) {
            Flash::error('Dron not found');

            return redirect(route('drons.index'));
        }

        $dron = $this->dronRepository->update($request->all(), $id);

        Flash::success('Dron updated successfully.');

        return redirect(route('drons.index'));
    }

    /**
     * Remove the specified dron from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dron = $this->dronRepository->findWithoutFail($id);

        if (empty($dron)) {
            Flash::error('Dron not found');

            return redirect(route('drons.index'));
        }

        $this->dronRepository->delete($id);

        Flash::success('Dron deleted successfully.');

        return redirect(route('drons.index'));
    }
}
