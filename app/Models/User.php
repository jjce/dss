<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version August 8, 2019, 3:11 am UTC
 *
 * @property string name
 * @property string email
 * @property string entity
 * @property string lastName
 * @property string phone
 * @property string celular
 * @property string website
 * @property string password
 * @property string remember_token
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'entity',
        'name',
        'lastName',
        'email',
        'phone',
        'celular',
        'website',
        'password',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'entity' => 'string',
        'lastName' => 'string',
        'phone' => 'string',
        'celular' => 'string',
        'website' => 'string',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'entity' => 'required',
        'lastName' => 'required',
        'password' => 'required'
    ];

    /*-------------------------------------- Accessors -------------------------------------------*/
    public function getFullNameAttribute()
    {
        return $this->name." ".$this->lastName;
    }
}
