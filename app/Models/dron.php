<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class dron
 * @package App\Models
 * @version June 11, 2020, 4:32 am UTC
 *
 * @property 1tM
 * @property string active
 * @property string Marca
 * @property string modelo
 * @property string descripcion
 * @property string serial
 * @property integer motores
 */
class dron extends Model
{
    use SoftDeletes;

    public $table = 'drons';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'active',
        'Marca',
        'modelo',
        'descripcion',
        'serial',
        'motores'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'string',
        'Marca' => 'string',
        'modelo' => 'string',
        'descripcion' => 'string',
        'serial' => 'string',
        'motores' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required',
        'Marca' => 'required|min:1|max:255',
        'modelo' => 'required|min:5|max:255',
        'serial' => 'required|min:5|max:255',
        'motores' => 'numeric'
    ];

    
}
