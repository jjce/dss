<?php

namespace App\Models\Members;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class reservationOrder
 * @package App\Models\Members
 * @version August 28, 2019, 5:56 am UTC
 *
 * @property \App\Models\Members\customer customer
 * @property \App\Models\Members\course course
 * @property integer active
 * @property integer purchased
 * @property integer customer_id
 * @property integer course_id
 */
class reservationOrder extends Model
{
    use SoftDeletes;

    public $table = 'reservation_orders';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'active',
        'purchased',
        'customer_id',
        'course_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'integer',
        'purchased' => 'integer',
        'customer_id' => 'integer',
        'course_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required|numeric|min:0|max:1',
        'purchased' => 'required|numeric|min:0|max:1',
        'customer_id' => 'numeric|min:1|required',
        'course_id' => 'numeric|min:1|required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customer()
    {
        return $this->belongsTo(\App\Models\Members\customer::class, 'customer_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Members\course::class, 'course_id', 'id');
    }
}
