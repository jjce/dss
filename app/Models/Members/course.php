<?php

namespace App\Models\Members;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class course
 * @package App\Models\Members
 * @version August 27, 2019, 5:32 am UTC
 *
 * @property \App\Models\Members\seller seller
 * @property string curso
 * @property string descripcion
 * @property integer duracion
 * @property integer min_estudiantes
 * @property integer max_estudiantes
 * @property string website
 * @property integer valor
 * @property string palabras_clave
 * @property integer activo
 * @property integer seller_id
 */
class course extends Model
{
    use SoftDeletes;

    public $table = 'courses';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'curso',
        'descripcion',
        'duracion',
        'min_estudiantes',
        'max_estudiantes',
        'website',
        'valor',
        'palabras_clave',
        'activo',
        'seller_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'curso' => 'string',
        'descripcion' => 'string',
        'duracion' => 'integer',
        'min_estudiantes' => 'integer',
        'max_estudiantes' => 'integer',
        'website' => 'string',
        'valor' => 'integer',
        'palabras_clave' => 'string',
        'activo' => 'integer',
        'seller_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'curso' => 'required|unique:cursos|max:255',
        'duracion' => 'required|numeric|min:1',
        'min_estudiantes' => 'required|numeric|min:1',
        'max_estudiantes' => 'required|numeric',
        'website' => 'max:255',
        'valor' => 'required|numeric',
        'palabras_clave' => 'required',
        'activo' => 'required|numeric|min:0|max:1'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function seller()
    {
        return $this->belongsTo(\App\Models\Members\seller::class, 'seller_id', 'id');
    }
}
