<?php

namespace App\Models\Members;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class seller
 * @package App\Models\Members
 * @version August 27, 2019, 5:08 am UTC
 *
 * @property \App\Models\Members\User user
 * @property integer active
 * @property integer user_id
 */
class seller extends Model
{
    use SoftDeletes;

    public $table = 'sellers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'active',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required|numeric|min:0|max:1',
        'user_id' => 'numeric|min:1|required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->hasOne(\App\Models\Members\User::class, 'user_id', 'id');
    }
}
