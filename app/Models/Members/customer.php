<?php

namespace App\Models\Members;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class customer
 * @package App\Models\Members
 * @version August 28, 2019, 4:15 am UTC
 *
 * @property \App\Models\Members\User user
 * @property integer active
 * @property integer user_id
 */
class customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'active',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required|numeric|min:0|max:1',
        'user_id' => 'numeric|min:1|required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->hasOne(\App\Models\Members\User::class, 'user_id', 'id');
    }
}
