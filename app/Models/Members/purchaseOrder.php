<?php

namespace App\Models\Members;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class purchaseOrder
 * @package App\Models\Members
 * @version August 28, 2019, 6:02 am UTC
 *
 * @property \App\Models\Members\customer customer
 * @property \App\Models\Members\course course
 * @property integer customer_id
 * @property integer course_id
 * @property string curso
 * @property string descripcion
 * @property integer duracion
 * @property integer min_estudiantes
 * @property integer max_estudiantes
 * @property integer num_estudiantes
 * @property string websiteCourse
 * @property integer valor
 * @property string palabras_clave
 * @property string entity
 * @property string name
 * @property string lastName
 * @property string email
 * @property string phone
 * @property string celular
 * @property string website
 */
class purchaseOrder extends Model
{
    use SoftDeletes;

    public $table = 'purchase_orders';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_id',
        'course_id',
        'curso',
        'descripcion',
        'duracion',
        'min_estudiantes',
        'max_estudiantes',
        'num_estudiantes',
        'websiteCourse',
        'valor',
        'palabras_clave',
        'entity',
        'name',
        'lastName',
        'email',
        'phone',
        'celular',
        'website'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'course_id' => 'integer',
        'curso' => 'string',
        'descripcion' => 'string',
        'duracion' => 'integer',
        'min_estudiantes' => 'integer',
        'max_estudiantes' => 'integer',
        'num_estudiantes' => 'integer',
        'websiteCourse' => 'string',
        'valor' => 'integer',
        'palabras_clave' => 'string',
        'entity' => 'string',
        'name' => 'string',
        'lastName' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'celular' => 'string',
        'website' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'numeric|min:1|required',
        'course_id' => 'numeric|min:1|required',
        'curso' => 'required|unique:cursos|max:255',
        'duracion' => 'required|numeric|min:1',
        'min_estudiantes' => 'required|numeric|min:1',
        'max_estudiantes' => 'required|numeric',
        'num_estudiantes' => 'required|numeric',
        'websiteCourse' => 'max:255',
        'valor' => 'required|numeric',
        'palabras_clave' => 'required',
        'entity' => 'required|string|max:255',
        'name' => 'required|string|max:255',
        'lastName' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
        'phone' => 'nullable|string|max:50',
        'celular' => 'nullable|string|max:50',
        'website' => 'nullable|string|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customer()
    {
        return $this->belongsTo(\App\Models\Members\customer::class, 'customer_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Members\course::class, 'course_id', 'id');
    }
}
