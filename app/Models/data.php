<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class data
 * @package App\Models
 * @version June 11, 2020, 6:07 am UTC
 *
 * @property \App\Models\Mission idMision
 * @property string latitud
 * @property string longitud
 * @property string altitud
 * @property string velocidad
 * @property string voltaje
 * @property string corriente
 * @property string bateria_nvl
 * @property integer id_mision
 */
class data extends Model
{
    use SoftDeletes;

    public $table = 'data';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'latitud',
        'longitud',
        'altitud',
        'velocidad',
        'voltaje',
        'corriente',
        'bateria_nvl',
        'id_mision'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'latitud' => 'string',
        'longitud' => 'string',
        'altitud' => 'string',
        'velocidad' => 'string',
        'voltaje' => 'string',
        'corriente' => 'string',
        'bateria_nvl' => 'string',
        'id_mision' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'latitud' => 'required|min:1|max:255',
        'longitud' => 'required|min:1|max:255',
        'altitud' => 'required|min:1|max:255',
        'velocidad' => 'required|min:1|max:255',
        'voltaje' => 'required|min:1|max:255',
        'corriente' => 'required|min:1|max:255',
        'bateria_nvl' => 'required|min:1|max:255'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idMision()
    {
        return $this->belongsTo(\App\Models\Mission::class, 'id_mision', 'id');
    }
}
