<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class mission
 * @package App\Models
 * @version June 11, 2020, 5:26 am UTC
 *
 * @property \App\Models\Dron dron
 * @property string active
 * @property string tipo_mision
 * @property string descripcion
 * @property string ruta
 * @property string trazabilidad
 * @property string|\Carbon\Carbon fecha_mision
 * @property string estado
 * @property integer dron_id
 */
class mission extends Model
{
    use SoftDeletes;

    public $table = 'missions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'active',
        'tipo_mision',
        'descripcion',
        'ruta',
        'trazabilidad',
        'fecha_mision',
        'estado',
        'dron_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'string',
        'tipo_mision' => 'string',
        'descripcion' => 'string',
        'ruta' => 'string',
        'trazabilidad' => 'string',
        'fecha_mision' => 'datetime',
        'estado' => 'string',
        'dron_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required',
        'tipo_mision' => 'required',
        'fecha_mision' => 'required',
        'estado' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function dron()
    {
        return $this->belongsTo(\App\Models\Dron::class, 'dron_id', 'id');
    }
}
