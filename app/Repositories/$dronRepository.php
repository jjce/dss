<?php

namespace App\Repositories;

use App\Models\$dron;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class $dronRepository
 * @package App\Repositories
 * @version June 11, 2020, 2:40 am UTC
 *
 * @method $dron findWithoutFail($id, $columns = ['*'])
 * @method $dron find($id, $columns = ['*'])
 * @method $dron first($columns = ['*'])
*/
class $dronRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Marca',
        'modelo',
        'serial'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return $dron::class;
    }
}
