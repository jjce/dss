<?php

namespace App\Repositories;

use App\Models\data;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class dataRepository
 * @package App\Repositories
 * @version June 11, 2020, 6:07 am UTC
 *
 * @method data findWithoutFail($id, $columns = ['*'])
 * @method data find($id, $columns = ['*'])
 * @method data first($columns = ['*'])
*/
class dataRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return data::class;
    }
}
