<?php

namespace App\Repositories;

use App\Models\mission;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class missionRepository
 * @package App\Repositories
 * @version June 11, 2020, 5:26 am UTC
 *
 * @method mission findWithoutFail($id, $columns = ['*'])
 * @method mission find($id, $columns = ['*'])
 * @method mission first($columns = ['*'])
*/
class missionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo_mision',
        'fecha_mision',
        'estado'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return mission::class;
    }
}
