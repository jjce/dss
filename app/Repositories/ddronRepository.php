<?php

namespace App\Repositories;

use App\Models\ddron;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ddronRepository
 * @package App\Repositories
 * @version June 11, 2020, 2:41 am UTC
 *
 * @method ddron findWithoutFail($id, $columns = ['*'])
 * @method ddron find($id, $columns = ['*'])
 * @method ddron first($columns = ['*'])
*/
class ddronRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Marca',
        'modelo',
        'serial'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ddron::class;
    }
}
