<?php

namespace App\Repositories\Members;

use App\Models\Members\seller;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class sellerRepository
 * @package App\Repositories\Members
 * @version August 27, 2019, 5:08 am UTC
 *
 * @method seller findWithoutFail($id, $columns = ['*'])
 * @method seller find($id, $columns = ['*'])
 * @method seller first($columns = ['*'])
*/
class sellerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return seller::class;
    }
}
