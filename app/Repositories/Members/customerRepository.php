<?php

namespace App\Repositories\Members;

use App\Models\Members\customer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class customerRepository
 * @package App\Repositories\Members
 * @version August 28, 2019, 4:15 am UTC
 *
 * @method customer findWithoutFail($id, $columns = ['*'])
 * @method customer find($id, $columns = ['*'])
 * @method customer first($columns = ['*'])
*/
class customerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return customer::class;
    }
}
