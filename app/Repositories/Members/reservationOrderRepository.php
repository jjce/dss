<?php

namespace App\Repositories\Members;

use App\Models\Members\reservationOrder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class reservationOrderRepository
 * @package App\Repositories\Members
 * @version August 28, 2019, 5:56 am UTC
 *
 * @method reservationOrder findWithoutFail($id, $columns = ['*'])
 * @method reservationOrder find($id, $columns = ['*'])
 * @method reservationOrder first($columns = ['*'])
*/
class reservationOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'course_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return reservationOrder::class;
    }
}
