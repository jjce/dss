<?php

namespace App\Repositories\Members;

use App\Models\Members\purchaseOrder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class purchaseOrderRepository
 * @package App\Repositories\Members
 * @version August 28, 2019, 6:02 am UTC
 *
 * @method purchaseOrder findWithoutFail($id, $columns = ['*'])
 * @method purchaseOrder find($id, $columns = ['*'])
 * @method purchaseOrder first($columns = ['*'])
*/
class purchaseOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'course_id',
        'curso',
        'palabras_clave'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return purchaseOrder::class;
    }
}
