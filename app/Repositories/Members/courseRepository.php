<?php

namespace App\Repositories\Members;

use App\Models\Members\course;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class courseRepository
 * @package App\Repositories\Members
 * @version August 27, 2019, 5:32 am UTC
 *
 * @method course findWithoutFail($id, $columns = ['*'])
 * @method course find($id, $columns = ['*'])
 * @method course first($columns = ['*'])
*/
class courseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'curso',
        'palabras_clave'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return course::class;
    }
}
