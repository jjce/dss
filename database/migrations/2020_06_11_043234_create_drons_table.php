<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDronsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('active');
            $table->string('Marca');
            $table->string('modelo')->nullable();
            $table->text('descripcion');
            $table->string('serial');
            $table->integer('motores');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drons');
    }
}
