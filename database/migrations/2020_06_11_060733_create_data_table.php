<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('latitud');
            $table->string('longitud');
            $table->string('altitud');
            $table->string('velocidad');
            $table->string('voltaje');
            $table->string('corriente');
            $table->string('bateria_nvl');
            $table->integer('id_mision')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_mision')->references('id')->on('missions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data');
    }
}
