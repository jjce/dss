<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchaseOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->string('curso', 255);
            $table->text('descripcion');
            $table->integer('duracion');
            $table->integer('min_estudiantes');
            $table->integer('max_estudiantes');
            $table->integer('num_estudiantes');
            $table->string('websiteCourse', 255)->nullable();
            $table->integer('valor');
            $table->string('palabras_clave', 255);
            $table->string('entity', 255);
            $table->string('name', 255);
            $table->string('lastName', 255);
            $table->string('email', 255);
            $table->string('phone', 50);
            $table->string('celular', 50);
            $table->string('website', 255);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('course_id')->references('id')->on('courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_orders');
    }
}
