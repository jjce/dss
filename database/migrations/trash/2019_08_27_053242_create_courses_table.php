<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('curso', 255);
            $table->text('descripcion');
            $table->integer('duracion');
            $table->integer('min_estudiantes');
            $table->integer('max_estudiantes');
            $table->string('website', 255)->nullable();
            $table->integer('valor');
            $table->string('palabras_clave', 255);
            $table->integer('activo')->default(1);
            $table->integer('seller_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('seller_id')->references('id')->on('sellers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
