<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMissionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('active');
            $table->string('tipo_mision');
            $table->text('descripcion')->nullable();
            $table->text('ruta')->nullable();
            $table->text('trazabilidad')->nullable();
            $table->datetime('fecha_mision');
            $table->string('estado');
            $table->integer('dron_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('dron_id')->references('id')->on('drons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('missions');
    }
}
