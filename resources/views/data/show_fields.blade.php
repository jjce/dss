<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $data->id !!}</p>
</div>

<!-- Latitud Field -->
<div class="form-group">
    {!! Form::label('latitud', 'Latitud:') !!}
    <p>{!! $data->latitud !!}</p>
</div>

<!-- Longitud Field -->
<div class="form-group">
    {!! Form::label('longitud', 'Longitud:') !!}
    <p>{!! $data->longitud !!}</p>
</div>

<!-- Altitud Field -->
<div class="form-group">
    {!! Form::label('altitud', 'Altitud:') !!}
    <p>{!! $data->altitud !!}</p>
</div>

<!-- Velocidad Field -->
<div class="form-group">
    {!! Form::label('velocidad', 'Velocidad:') !!}
    <p>{!! $data->velocidad !!}</p>
</div>

<!-- Voltaje Field -->
<div class="form-group">
    {!! Form::label('voltaje', 'Voltaje:') !!}
    <p>{!! $data->voltaje !!}</p>
</div>

<!-- Corriente Field -->
<div class="form-group">
    {!! Form::label('corriente', 'Corriente:') !!}
    <p>{!! $data->corriente !!}</p>
</div>

<!-- Bateria Nvl Field -->
<div class="form-group">
    {!! Form::label('bateria_nvl', 'Bateria Nvl:') !!}
    <p>{!! $data->bateria_nvl !!}</p>
</div>

<!-- Id Mision Field -->
<div class="form-group">
    {!! Form::label('id_mision', 'Id Mision:') !!}
    <p>{!! $data->id_mision !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $data->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $data->updated_at !!}</p>
</div>

