<!-- Latitud Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitud', 'Latitud:') !!}
    {!! Form::text('latitud', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitud Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitud', 'Longitud:') !!}
    {!! Form::text('longitud', null, ['class' => 'form-control']) !!}
</div>

<!-- Altitud Field -->
<div class="form-group col-sm-6">
    {!! Form::label('altitud', 'Altitud:') !!}
    {!! Form::text('altitud', null, ['class' => 'form-control']) !!}
</div>

<!-- Velocidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('velocidad', 'Velocidad:') !!}
    {!! Form::text('velocidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Voltaje Field -->
<div class="form-group col-sm-6">
    {!! Form::label('voltaje', 'Voltaje:') !!}
    {!! Form::text('voltaje', null, ['class' => 'form-control']) !!}
</div>

<!-- Corriente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('corriente', 'Corriente:') !!}
    {!! Form::text('corriente', null, ['class' => 'form-control']) !!}
</div>

<!-- Bateria Nvl Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bateria_nvl', 'Bateria Nvl:') !!}
    {!! Form::text('bateria_nvl', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Mision Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_mision', 'Id Mision:') !!}
    {!! Form::text('id_mision', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('data.index') !!}" class="btn btn-default">Cancel</a>
</div>
