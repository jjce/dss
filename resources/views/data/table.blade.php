<section class="content-header">
    <h3 class="pull-left">Trazabilidad</h3>

</section>
<div class="content">
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table" id="data-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Latitud</th>
                        <th>Longitud</th>
                        <th>Altitud</th>
                        <th>Velocidad</th>
                        <th>Voltaje</th>
                        <th>Corriente</th>
                        <th>Bateria Nvl</th>
                        <th hidden>Id Mision</th>
                        <th colspan="3" hidden>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($data))
                        @foreach($data as $data)
                            <tr>
                                <td>{!! $data->id !!}</td>
                                <td>{!! $data->latitud !!}</td>
                                <td>{!! $data->longitud !!}</td>
                                <td>{!! $data->altitud !!}</td>
                                <td>{!! $data->velocidad !!}</td>
                                <td>{!! $data->voltaje !!}</td>
                                <td>{!! $data->corriente !!}</td>
                                <td>{!! $data->bateria_nvl !!}</td>
                                <td hidden>{!! $data->id_mision !!}</td>
                                <td hidden>
                                    {!! Form::open(['route' => ['data.destroy', $data->id], 'method' => 'delete']) !!}
                                    <div class='btn-group'>
                                        <a href="{!! route('data.show', [$data->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        <a href="{!! route('data.edit', [$data->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" class="text-center ">
                                No hay datos registrados
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="text-center">

    </div>
</div>
