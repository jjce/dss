<div class="container-fluid">
<fieldset>
    <legend>Información de la institución educativa</legend>

    <!-- Entity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('entity', 'Entity:') !!}
    {!! Form::text('entity', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    <div class=" input-group">
        <span class="input-group-addon" id="basic-addon3">https://</span>
        {!! Form::text('website', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>
</fieldset>
    <fieldset>
        <legend>Información de contacto</legend>

    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Lastname Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('lastName', 'Lastname:') !!}
        {!! Form::text('lastName', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Phone Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone', 'Phone:') !!}
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Celular Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('celular', 'Celular:') !!}
        {!! Form::text('celular', null, ['class' => 'form-control']) !!}
    </div>

        <!-- Activo Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('active', 'Activo:') !!}
            <label class="radio-inline">
                {!! Form::radio('active', 1, null) !!} Activo
            </label>

            <label class="radio-inline">
                {!! Form::radio('active', 0, null) !!} Inactivo
            </label>
        </div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>
</fieldset>
</div>
