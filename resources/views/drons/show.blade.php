@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Dron <a href="{!! route('missions.index') !!}" class="btn btn-default pull-right">Gst. misiones</a>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('drons.show_fields')
                    <div class="col-md-12">
                    <a href="{!! route('drons.index') !!}" class="btn btn-default">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
