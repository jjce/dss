<div class="table-responsive">
    <table class="table" id="drons-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Active</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Serial</th>
        <th>Motores</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($drons as $dron)
            <tr>
                <td>{!! $dron->id !!}</td>
            <td>{!! $dron->active !!}</td>
            <td>{!! $dron->Marca !!}</td>
            <td>{!! $dron->modelo !!}</td>
            <td>{!! $dron->serial !!}</td>
            <td>{!! $dron->motores !!}</td>
                <td>
                    {!! Form::open(['route' => ['drons.destroy', $dron->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('drons.show', [$dron->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('drons.edit', [$dron->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
