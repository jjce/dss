<!-- Active Field -->
<div class="form-group col-sm-12">
    {!! Form::label('active', 'Active:') !!}
    <label class="radio-inline">
        {!! Form::radio('active', "Activo", true) !!} Activo
    </label>

    <label class="radio-inline">
        {!! Form::radio('active', "Inactivo", null) !!} Inactivo
    </label>

</div>

<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Marca', 'Marca:') !!}
    <?php
    if(!isset($dron))  {
        $temp ='Maverick';

    }else $temp=null; ?>
    {!! Form::select('Marca', ['DJI' => 'DJI', 'Maverick' => 'Maverick', 'Parrot' => 'Parrot', 'Walkera' => 'Walkera', 'Yuneec' => 'Yuneec'], $temp, ['class' => 'form-control']) !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control','rows'=>'3']) !!}
</div>

<!-- Serial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serial', 'Serial:') !!}
    {!! Form::text('serial', null, ['class' => 'form-control','rows'=>'3']) !!}
</div>

<!-- Motores Field -->
<div class="form-group col-sm-6">
    {!! Form::label('motores', 'Motores:') !!}
    {!! Form::number('motores', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('drons.index') !!}" class="btn btn-default">Cancel</a>
</div>
