<!-- Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('id', 'Id:') !!}
    {!! $dron->id !!}
</div>

<!-- Active Field -->
<div class="form-group col-md-6">
    <b>{!! Form::label('active', 'Active:') !!}</b>
    {!! $dron->active !!}
</div>

<!-- Marca Field -->
<div class="form-group col-md-6">
    {!! Form::label('Marca', 'Marca:') !!}
    {!! $dron->Marca !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-md-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! $dron->modelo !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-md-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! $dron->descripcion !!}
</div>

<!-- Serial Field -->
<div class="form-group col-md-6">
    {!! Form::label('serial', 'Serial:') !!}
    {!! $dron->serial !!}
</div>

<!-- Motores Field -->
<div class="form-group col-md-6">
    {!! Form::label('motores', 'Motores:') !!}
    {!! $dron->motores !!}
</div>

<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! $dron->created_at !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! $dron->updated_at !!}
</div>

