@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Dron <a href="{!! route('missions.index',['dron_id'=>$dron->id]) !!}" class="btn btn-default pull-right">Gst. misiones</a>
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dron, ['route' => ['drons.update', $dron->id], 'method' => 'patch']) !!}

                        @include('drons.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection