<div class="table-responsive">
    <table class="table" id="sellers-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Active</th>
        <th>User Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($sellers as $seller)
            <tr>
                <td>{!! $seller->id !!}</td>
            <td>{!! $seller->active !!}</td>
            <td>{!! $seller->user_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['members.sellers.destroy', $seller->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('members.sellers.show', [$seller->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('members.sellers.edit', [$seller->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
