<!-- Active Field -->
<div class="form-group col-sm-12">
    {!! Form::label('active', 'Active:') !!}
    <label class="radio-inline">
        {!! Form::radio('active', "1", null) !!} Activo
    </label>

    <label class="radio-inline">
        {!! Form::radio('active', "0", null) !!} Inactivo
    </label>

</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('members.sellers.index') !!}" class="btn btn-default">Cancel</a>
</div>
