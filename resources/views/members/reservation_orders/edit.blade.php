@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reservation Order
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($reservationOrder, ['route' => ['members.reservationOrders.update', $reservationOrder->id], 'method' => 'patch']) !!}

                        @include('members.reservation_orders.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection