<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reservationOrder->id !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $reservationOrder->active !!}</p>
</div>

<!-- Expired At Field -->
<div class="form-group">
    {!! Form::label('expired_at', 'Expired At:') !!}
    <p>{!! $reservationOrder->expired_at !!}</p>
</div>

<!-- Purchased Field -->
<div class="form-group">
    {!! Form::label('purchased', 'Purchased:') !!}
    <p>{!! $reservationOrder->purchased !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $reservationOrder->customer_id !!}</p>
</div>

<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{!! $reservationOrder->course_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $reservationOrder->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $reservationOrder->updated_at !!}</p>
</div>

