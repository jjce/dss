<div class="table-responsive">
    <table class="table" id="reservationOrders-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Active</th>
        <th>Purchased</th>
        <th>Customer Id</th>
        <th>Course Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reservationOrders as $reservationOrder)
            <tr>
                <td>{!! $reservationOrder->id !!}</td>
            <td>{!! $reservationOrder->active !!}</td>
            <td>{!! $reservationOrder->purchased !!}</td>
            <td>{!! $reservationOrder->customer_id !!}</td>
            <td>{!! $reservationOrder->course_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['members.reservationOrders.destroy', $reservationOrder->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('members.reservationOrders.show', [$reservationOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('members.reservationOrders.edit', [$reservationOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
