@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reservation Order
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'members.reservationOrders.store']) !!}

                        @include('members.reservation_orders.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
