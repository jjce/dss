<!-- Active Field -->
<div class="form-group col-sm-12">
    {!! Form::label('active', 'Active:') !!}
    <label class="radio-inline">
        {!! Form::radio('active', "1", null) !!} Activo
    </label>

    <label class="radio-inline">
        {!! Form::radio('active', "0", null) !!} Inactivo
    </label>

</div>

<!-- Purchased Field -->
<div class="form-group col-sm-12">
    {!! Form::label('purchased', 'Purchased:') !!}
    <label class="radio-inline">
        {!! Form::radio('purchased', "1", null) !!} Activo
    </label>

    <label class="radio-inline">
        {!! Form::radio('purchased', "0", null) !!} Inactivo
    </label>

</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('members.reservationOrders.index') !!}" class="btn btn-default">Cancel</a>
</div>
