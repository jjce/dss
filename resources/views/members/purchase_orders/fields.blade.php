<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Curso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('curso', 'Curso:') !!}
    {!! Form::text('curso', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Duracion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duracion', 'Duracion:') !!}
    {!! Form::number('duracion', null, ['class' => 'form-control']) !!}
</div>

<!-- Min Estudiantes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_estudiantes', 'Min Estudiantes:') !!}
    {!! Form::number('min_estudiantes', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Estudiantes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_estudiantes', 'Max Estudiantes:') !!}
    {!! Form::number('max_estudiantes', null, ['class' => 'form-control']) !!}
</div>

<!-- Num Estudiantes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('num_estudiantes', 'Num Estudiantes:') !!}
    {!! Form::number('num_estudiantes', null, ['class' => 'form-control']) !!}
</div>

<!-- Websitecourse Field -->
<div class="form-group col-sm-6">
    {!! Form::label('websiteCourse', 'Websitecourse:') !!}
    {!! Form::text('websiteCourse', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor', 'Valor:') !!}
    {!! Form::number('valor', null, ['class' => 'form-control']) !!}
</div>

<!-- Palabras Clave Field -->
<div class="form-group col-sm-6">
    {!! Form::label('palabras_clave', 'Palabras Clave:') !!}
    {!! Form::text('palabras_clave', null, ['class' => 'form-control']) !!}
</div>

<!-- Entity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('entity', 'Entity:') !!}
    {!! Form::text('entity', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastName', 'Lastname:') !!}
    {!! Form::text('lastName', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Celular Field -->
<div class="form-group col-sm-6">
    {!! Form::label('celular', 'Celular:') !!}
    {!! Form::text('celular', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('members.purchaseOrders.index') !!}" class="btn btn-default">Cancel</a>
</div>
