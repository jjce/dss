<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $purchaseOrder->id !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $purchaseOrder->customer_id !!}</p>
</div>

<!-- Course Id Field -->
<div class="form-group">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{!! $purchaseOrder->course_id !!}</p>
</div>

<!-- Curso Field -->
<div class="form-group">
    {!! Form::label('curso', 'Curso:') !!}
    <p>{!! $purchaseOrder->curso !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $purchaseOrder->descripcion !!}</p>
</div>

<!-- Duracion Field -->
<div class="form-group">
    {!! Form::label('duracion', 'Duracion:') !!}
    <p>{!! $purchaseOrder->duracion !!}</p>
</div>

<!-- Min Estudiantes Field -->
<div class="form-group">
    {!! Form::label('min_estudiantes', 'Min Estudiantes:') !!}
    <p>{!! $purchaseOrder->min_estudiantes !!}</p>
</div>

<!-- Max Estudiantes Field -->
<div class="form-group">
    {!! Form::label('max_estudiantes', 'Max Estudiantes:') !!}
    <p>{!! $purchaseOrder->max_estudiantes !!}</p>
</div>

<!-- Num Estudiantes Field -->
<div class="form-group">
    {!! Form::label('num_estudiantes', 'Num Estudiantes:') !!}
    <p>{!! $purchaseOrder->num_estudiantes !!}</p>
</div>

<!-- Websitecourse Field -->
<div class="form-group">
    {!! Form::label('websiteCourse', 'Websitecourse:') !!}
    <p>{!! $purchaseOrder->websiteCourse !!}</p>
</div>

<!-- Valor Field -->
<div class="form-group">
    {!! Form::label('valor', 'Valor:') !!}
    <p>{!! $purchaseOrder->valor !!}</p>
</div>

<!-- Palabras Clave Field -->
<div class="form-group">
    {!! Form::label('palabras_clave', 'Palabras Clave:') !!}
    <p>{!! $purchaseOrder->palabras_clave !!}</p>
</div>

<!-- Entity Field -->
<div class="form-group">
    {!! Form::label('entity', 'Entity:') !!}
    <p>{!! $purchaseOrder->entity !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $purchaseOrder->name !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastName', 'Lastname:') !!}
    <p>{!! $purchaseOrder->lastName !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $purchaseOrder->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $purchaseOrder->phone !!}</p>
</div>

<!-- Celular Field -->
<div class="form-group">
    {!! Form::label('celular', 'Celular:') !!}
    <p>{!! $purchaseOrder->celular !!}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{!! $purchaseOrder->website !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $purchaseOrder->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $purchaseOrder->updated_at !!}</p>
</div>

