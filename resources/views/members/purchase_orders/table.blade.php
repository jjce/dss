<div class="table-responsive">
    <table class="table" id="purchaseOrders-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Curso</th>
        <th>Descripcion</th>
        <th>Duracion</th>
        <th>Num Estudiantes</th>
        <th>Valor</th>
        <th>Entity</th>
        <th>Created At</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($purchaseOrders as $purchaseOrder)
            <tr>
                <td>{!! $purchaseOrder->id !!}</td>
            <td>{!! $purchaseOrder->curso !!}</td>
            <td>{!! $purchaseOrder->descripcion !!}</td>
            <td>{!! $purchaseOrder->duracion !!}</td>
            <td>{!! $purchaseOrder->num_estudiantes !!}</td>
            <td>{!! $purchaseOrder->valor !!}</td>
            <td>{!! $purchaseOrder->entity !!}</td>
            <td>{!! $purchaseOrder->created_at !!}</td>
                <td>
                    {!! Form::open(['route' => ['members.purchaseOrders.destroy', $purchaseOrder->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('members.purchaseOrders.show', [$purchaseOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('members.purchaseOrders.edit', [$purchaseOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
