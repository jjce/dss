<!-- Curso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('curso', 'Curso:') !!}
    {!! Form::text('curso', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Duracion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duracion', 'Duracion:') !!}
    {!! Form::number('duracion', null, ['class' => 'form-control']) !!}
</div>

<!-- Min Estudiantes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_estudiantes', 'Min Estudiantes:') !!}
    {!! Form::number('min_estudiantes', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Estudiantes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_estudiantes', 'Max Estudiantes:') !!}
    {!! Form::number('max_estudiantes', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor', 'Valor:') !!}
    {!! Form::number('valor', null, ['class' => 'form-control']) !!}
</div>

<!-- Palabras Clave Field -->
<div class="form-group col-sm-6">
    {!! Form::label('palabras_clave', 'Palabras Clave:') !!}
    {!! Form::text('palabras_clave', null, ['class' => 'form-control']) !!}
</div>

<!-- Activo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('activo', 'Activo:') !!}
    <label class="radio-inline">
        {!! Form::radio('activo', "1", null) !!} Activo
    </label>

    <label class="radio-inline">
        {!! Form::radio('activo', "0", null) !!} Inactivo
    </label>

</div>

<!-- Seller Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    {!! Form::text('seller_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('members.courses.index') !!}" class="btn btn-default">Cancel</a>
</div>
