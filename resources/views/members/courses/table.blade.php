<div class="table-responsive">
    <table class="table" id="courses-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Curso</th>
        <th>Descripcion</th>
        <th>Duracion</th>
        <th>Min Estudiantes</th>
        <th>Max Estudiantes</th>
        <th>Valor</th>
        <th>Activo</th>
        <th>Seller Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($courses as $course)
            <tr>
                <td>{!! $course->id !!}</td>
            <td>{!! $course->curso !!}</td>
            <td>{!! $course->descripcion !!}</td>
            <td>{!! $course->duracion !!}</td>
            <td>{!! $course->min_estudiantes !!}</td>
            <td>{!! $course->max_estudiantes !!}</td>
            <td>{!! $course->valor !!}</td>
            <td>{!! $course->activo !!}</td>
            <td>{!! $course->seller_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['members.courses.destroy', $course->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('members.courses.show', [$course->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('members.courses.edit', [$course->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
