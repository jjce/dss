<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $course->id !!}</p>
</div>

<!-- Curso Field -->
<div class="form-group">
    {!! Form::label('curso', 'Curso:') !!}
    <p>{!! $course->curso !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $course->descripcion !!}</p>
</div>

<!-- Duracion Field -->
<div class="form-group">
    {!! Form::label('duracion', 'Duracion:') !!}
    <p>{!! $course->duracion !!}</p>
</div>

<!-- Min Estudiantes Field -->
<div class="form-group">
    {!! Form::label('min_estudiantes', 'Min Estudiantes:') !!}
    <p>{!! $course->min_estudiantes !!}</p>
</div>

<!-- Max Estudiantes Field -->
<div class="form-group">
    {!! Form::label('max_estudiantes', 'Max Estudiantes:') !!}
    <p>{!! $course->max_estudiantes !!}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{!! $course->website !!}</p>
</div>

<!-- Valor Field -->
<div class="form-group">
    {!! Form::label('valor', 'Valor:') !!}
    <p>{!! $course->valor !!}</p>
</div>

<!-- Palabras Clave Field -->
<div class="form-group">
    {!! Form::label('palabras_clave', 'Palabras Clave:') !!}
    <p>{!! $course->palabras_clave !!}</p>
</div>

<!-- Activo Field -->
<div class="form-group">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{!! $course->activo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $course->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $course->updated_at !!}</p>
</div>

<!-- Seller Id Field -->
<div class="form-group">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    <p>{!! $course->seller_id !!}</p>
</div>

