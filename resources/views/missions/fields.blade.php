<!-- Active Field -->
<div class="form-group col-sm-12">
    {!! Form::label('active', 'Active:') !!}
    <label class="radio-inline">
        {!! Form::radio('active', "Activo", true) !!} Activo
    </label>

    <label class="radio-inline">
        {!! Form::radio('active', "Activo", null) !!} Inactivo
    </label>

</div>

<!-- Tipo Mision Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_mision', 'Tipo Mision:') !!}
    {!! Form::select('tipo_mision', ['Entrega' => 'Entrega', 'Vigilancia' => 'Vigilancia', 'Eventos' => 'Eventos', 'Aseo' => 'Aseo'], null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows'=>'3']) !!}
</div>

<!-- Ruta Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ruta', 'Ruta:') !!}
    {!! Form::textarea('ruta', null, ['class' => 'form-control', 'rows'=>'3']) !!}
</div>

<!-- Trazabilidad Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('trazabilidad', 'Trazabilidad:') !!}
    {!! Form::textarea('trazabilidad', null, ['class' => 'form-control', 'rows'=>'3']) !!}
</div>

<!-- Fecha Mision Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_mision', 'Fecha Mision:') !!}
    <?php
    $fecha = isset($mission->fecha_mision)?$mission->fecha_mision:null;

    ?>
    {!! Form::date('fecha_mision', $fecha, ['class' => 'form-control','id'=>'fecha_mision']) !!}

</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_mision').datetimepicker({

            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>
@endsection

<!-- Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::select('estado', ['Ocupado' => 'Ocupado', 'Bueno' => 'Bueno', 'Malo' => 'Malo', 'Mantinimiento' => 'Mantinimiento', 'Perdido' => 'Perdido'], null, ['class' => 'form-control']) !!}
</div>

<!-- Dron Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dron_id', 'Dron Id:') !!}
    {!! Form::text('dron_id', null, ['class' => 'form-control']) !!}

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('missions.index') !!}" class="btn btn-default">Cancel</a>
</div>
