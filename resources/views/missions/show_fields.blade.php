<!-- Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('id', 'Id:') !!}
    {!! $mission->id !!}
</div>

<!-- Active Field -->
<div class="form-group col-md-6">
    {!! Form::label('active', 'Active:') !!}
    {!! $mission->active !!}
</div>

<!-- Tipo Mision Field -->
<div class="form-group col-md-6">
    {!! Form::label('tipo_mision', 'Tipo Mision:') !!}
    {!! $mission->tipo_mision !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-md-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! $mission->descripcion !!}
</div>

<!-- Ruta Field -->
<div class="form-group col-md-6">
    {!! Form::label('ruta', 'Ruta:') !!}
    {!! $mission->ruta !!}
</div>

<!-- Trazabilidad Field -->
<div class="form-group col-md-6">
    {!! Form::label('trazabilidad', 'Trazabilidad:') !!}
    {!! $mission->trazabilidad !!}
</div>

<!-- Fecha Mision Field -->
<div class="form-group col-md-6">
    {!! Form::label('fecha_mision', 'Fecha Mision:') !!}
    {!! $mission->fecha_mision !!}
</div>

<!-- Estado Field -->
<div class="form-group col-md-6">
    {!! Form::label('estado', 'Estado:') !!}
    {!! $mission->estado !!}
</div>

<!-- Dron Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('dron_id', 'Dron Id:') !!}
    {!! $mission->dron_id !!}
</div>

<div class="form-group col-md-6" style="color: white">
    {!! Form::label('dron_id', '.') !!}
</div>


<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! $mission->created_at->format('d-M-yy h:i:s') !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! $mission->updated_at->format('d-M-yy h:i:s') !!}
</div>


