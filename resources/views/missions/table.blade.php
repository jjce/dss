<div class="table-responsive">
    <table class="table" id="missions-table">
        <thead>
            <tr>
                <th>Id</th>
        <th>Active</th>
        <th>Tipo Mision</th>
        <th>Fecha Mision</th>
        <th>Estado</th>
        <th>Dron Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($missions as $mission)
            <tr>
                <td>{!! $mission->id !!}</td>
            <td>{!! $mission->active !!}</td>
            <td>{!! $mission->tipo_mision !!}</td>
            <td>{!! $mission->fecha_mision !!}</td>
            <td>{!! $mission->estado !!}</td>
            <td>{!! $mission->dron_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['missions.destroy', $mission->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('missions.show', [$mission->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('missions.edit', [$mission->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
