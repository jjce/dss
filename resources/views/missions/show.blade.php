@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Mission
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('missions.show_fields')
                    <div class="col-md-12">
                        <a href="{!! route('missions.index') !!}" class="btn btn-default">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('data.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
